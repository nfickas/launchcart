package org.launchcode.launchcart.models;


import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * Created by LaunchCode
 */
@Entity
public class Item extends AbstractEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @ManyToOne
    private Cart cart;

    private String name;

    private double price;

    private String description;

    @NotNull
    private boolean newItem;

    public Item() {

    }

    public Item (String name, double price, String description) {
        this.name = name;
        this.price = price;
        this.description = description;
    }

    public Item(String name, double price) {
        this(name, price, "");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public boolean isNewItem() {
        return newItem;
    }

    public void setNewItem(boolean newItem) {
        this.newItem = newItem;
    }
}
