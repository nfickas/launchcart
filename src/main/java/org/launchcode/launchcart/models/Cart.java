package org.launchcode.launchcart.models;


import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by LaunchCode
 */
@Entity
public class Cart {

    @Id
    @NotNull
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @OneToMany
    @JoinColumn(name = "cart_id")
    private List<Item> items = new ArrayList<>();

    public List<Item> getItems() {
        return items;
    }

    public void addItem(Item item) {
        items.add(item);
    }

    public double computeTotal(){
        double total = 0;
        for(Item i: items){
            total += i.getPrice();
        }
        return total;
    }

    public void removeItem(Item item){
        items.remove(item);
    }

    public int getId() {
        return id;
    }
}
