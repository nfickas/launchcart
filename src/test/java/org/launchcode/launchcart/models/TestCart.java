package org.launchcode.launchcart.models;

import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

/**
 * Created by LaunchCode
 */
public class TestCart {

    private Cart cart;

    @Before
    public void setupCart() {
        cart = new Cart();
    }

    @Test
    public void testAddItem() {
        Item item = new Item("Test Item", 5);
        assertFalse(cart.getItems().contains(item));
        cart.addItem(item);
        assertTrue(cart.getItems().contains(item));
    }

    @Test
    public void testComputeTotal(){
        Item item = new Item("Test Item", 5);
        Item item2 = new Item("Test Item 2", 6);
        cart.addItem(item);
        assertEquals(5, cart.computeTotal(), .01d);
        cart.addItem(item2);
        assertEquals(11, cart.computeTotal(), .01d);

    }

    @Test
    public void testRemoveItem(){
        Item item = new Item("Test Item", 5);
        cart.addItem(item);
        assertTrue(cart.getItems().contains(item));
        cart.removeItem(item);
        assertFalse(cart.getItems().contains(item));
    }
}
