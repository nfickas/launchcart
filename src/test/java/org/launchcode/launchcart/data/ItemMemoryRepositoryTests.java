package org.launchcode.launchcart.data;

import org.junit.Before;
import org.junit.Test;
import org.launchcode.launchcart.models.Item;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by LaunchCode
 */
public class ItemMemoryRepositoryTests {

    private ItemMemoryRepository itemMemoryRepository;

    @Before
    public void setupCart() {
       itemMemoryRepository = new ItemMemoryRepository();
       itemMemoryRepository.clear();
    }

    //TODO: test that ItemMemoryRepository can...
    // 1) save multiple items
    // 2) each item has a unique uid
    // 3) you can find the item by it's uid
    @Test
    public void testAddItems() {
        Item item1 = new Item("Item 1", 5.0);
        Item item2 = new Item("Item 2", 6.0);
        Item item3 = new Item("Item 3", 7.0);

        itemMemoryRepository.save(item1);
        itemMemoryRepository.save(item2);
        itemMemoryRepository.save(item3);

        assertTrue("able to find item by it's uid", itemMemoryRepository.findOne(item1.getId()) == item1);
        assertTrue("ids should be unique", itemMemoryRepository.findAll().get(0).getId() != itemMemoryRepository.findAll().get(1).getId());


    }
}
